package forcomp
import Anagrams._;

object test {
  //sentence anagrams
  def sa(occurrences: Occurrences): List[Sentence]  = occurrences match {
  	case Nil => List(List())
  	case _ =>
	  for {
	  	occ <- combinations(occurrences)
	  	word <- dictionaryByOccurrences.getOrElse(occ, List() : List[Word])
	  	//otherWords <- List(dictionaryByOccurrences.getOrElse(subtract(occ,wordOccurrences(word)), List() : List[Word]))
	  	//if (sa(subtract(occ,wordOccurrences(word))) != List())
	  	//otherWords <- List(List("ah"), List("ha"))
	  	otherWords <- sa(subtract(occurrences,wordOccurrences(word)))
	  	//if (otherWords != Nil || subtract(occ,wordOccurrences(word)) == Nil)
	  } yield word::otherWords
	  
  }                                               //> sa: (occurrences: forcomp.Anagrams.Occurrences)List[forcomp.Anagrams.Sentenc
                                                  //| e]
  val occ = sentenceOccurrences(List("go", "ha")) //> occ  : forcomp.Anagrams.Occurrences = List((a,1), (g,1), (h,1), (o,1))
  val ss = sa(occ)                                //> ss  : List[forcomp.Anagrams.Sentence] = List(List(ah, go), List(ha, go), Lis
                                                  //| t(go, ah), List(go, ha))
	val sub = subtract(occ, wordOccurrences("ah"))
                                                  //> sub  : forcomp.Anagrams.Occurrences = List((g,1), (o,1))
  val satest= sa(sub)                             //> satest  : List[forcomp.Anagrams.Sentence] = List(List(go))
  val ow = List(dictionaryByOccurrences.getOrElse(sub, List() : List[Word]))
                                                  //> ow  : List[List[forcomp.Anagrams.Word]] = List(List(go))
}