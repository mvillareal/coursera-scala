package example

import common._

object Lists {
  /**
   * This method computes the sum of all elements in the list xs. There are
   * multiple techniques that can be used for implementing this method, and
   * you will learn during the class.
   *
   * For this example assignment you can use the following methods in class
   * `List`:
   *
   *  - `xs.isEmpty: Boolean` returns `true` if the list `xs` is empty
   *  - `xs.head: Int` returns the head element of the list `xs`. If the list
   *    is empty an exception is thrown
   *  - `xs.tail: List[Int]` returns the tail of the list `xs`, i.e. the the
   *    list `xs` without its `head` element
   *
   *  ''Hint:'' instead of writing a `for` or `while` loop, think of a recursive
   *  solution.
   *
   * @param xs A list of natural numbers
   * @return The sum of all elements in `xs`
   */
  def sum(xs: List[Int]): Int = 
    if (xs.isEmpty)
      0;
    else
      xs.head + sum(xs.tail);

  /**
   * This method returns the largest element in a list of integers. If the
   * list `xs` is empty it throws a `java.util.NoSuchElementException`.
   *
   * You can use the same methods of the class `List` as mentioned above.
   *
   * ''Hint:'' Again, think of a recursive solution instead of using looping
   * constructs. You might need to define an auxiliary method.
   *
   * @param xs A list of natural numbers
   * @return The largest element in `xs`
   * @throws java.util.NoSuchElementException if `xs` is an empty list
   */
  def max(xs: List[Int]): Int = 
    if (xs.isEmpty)
      throw new java.util.NoSuchElementException();
    else
    {
      if (xs.tail.isEmpty)
        xs.head;
      else
      {
        if (xs.head > max(xs.tail))
          xs.head;
        else
          max(xs.tail);
      }
    }

    //99 scala - http://aperiodic.net/phil/scala/s-99/

    //P01 (*) Find the last element of a list.
    def last(xs: List[Int]): Int =
      if (xs.tail.isEmpty)
        xs.head
      else 
        last(xs.tail)

    //P02 (*) Find the last but one element of a list.
    def penultimate(xs: List[Int]): Int =
      if (xs.tail.tail.isEmpty)
        xs.head
      else
        penultimate(xs.tail)

    //P03 (*) Find the Kth element of a list.
    def nth(ndx: Int, xs: List[Int]) : Int =
      if (ndx == 0 || xs.tail.isEmpty)
        xs.head
      else
        nth(ndx-1, xs.tail)

    //P04 (*) Find the number of elements of a list.
    def length(xs: List[Int]): Int = {
      def lengthAcc(xs: List[Int], acc: Int) : Int = 
        if (xs.tail.isEmpty)
          acc + 1
        else
          lengthAcc(xs.tail, acc + 1)

      if (xs.isEmpty)
        0
      else
        lengthAcc(xs, 0)
    }

    //P05 (*) Reverse a list.
    def reverse(xs: List[Int]): List[Int] = {
      def reverseAcc(xs: List[Int], acc:List[Int]) : List[Int] =
        if (xs.tail.isEmpty)
          xs.head::acc
        else
          reverseAcc(xs.tail, xs.head::acc)

      reverseAcc(xs, Nil)
    }

    //P06 (*) Find out whether a list is a palindrome.
    def isPalindrome(xs: List[Int]): Boolean =
      xs == reverse(xs)

    //P07 (**) Flatten a nested list structure.
    def flatten(xs: List[Any]): List[Any] = {
      def flattenAcc(xs: List[Any], acc: List[Any]) : List[Any] = {
        if (xs == Nil)
          acc
        else
        {
          val flatAcc = xs.head match {
            case (l:List[_])   => flattenAcc(l, acc) ::: acc
            case hd        => acc :+ hd
          }
          if (xs.tail.isEmpty)
            flatAcc
          else
            flattenAcc(xs.tail, acc:::flatAcc)
        }
      }

      flattenAcc(xs, Nil)
    }
}
