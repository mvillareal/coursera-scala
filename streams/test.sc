package streams
import Bloxorz._


object test
{
  val lev0 = Level0                               //> lev0  : streams.Bloxorz.Level0.type = streams.Bloxorz$Level0$@1f4384c2
  lev0.startPos                                   //> res0: streams.test.lev0.Pos = Pos(1,2)
  lev0.goal                                       //> res1: streams.test.lev0.Pos = Pos(1,3)
  lev0.pathsFromStart.take(4).toList              //> res2: List[(streams.test.lev0.Block, List[streams.test.lev0.Move])] = List((
                                                  //| Block(Pos(1,2),Pos(1,2)),List()), (Block(Pos(2,2),Pos(3,2)),List(Down)), (Bl
                                                  //| ock(Pos(2,3),Pos(3,3)),List(Right, Down)), (Block(Pos(1,3),Pos(1,3)),List(Up
                                                  //| , Right, Down)))
  lev0.pathsToGoal.take(1)(0)                     //> res3: (streams.test.lev0.Block, List[streams.test.lev0.Move]) = (Block(Pos(1
                                                  //| ,3),Pos(1,3)),List(Up, Right, Down))
	val lev1 = Level1                         //> lev1  : streams.Bloxorz.Level1.type = streams.Bloxorz$Level1$@7ffc6e42
	lev1.done(lev1.startBlock)                //> res4: Boolean = false
	lev1.goal                                 //> res5: streams.test.lev1.Pos = Pos(4,7)
	//lev1.pathsFromStart.take(20).toList
  //lev1.pathsFromStart.filter{_._1.b1.y==7}.take(12).toList foreach println
  lev1.pathsToGoal.take(1).toList                 //> res6: List[(streams.test.lev1.Block, List[streams.test.lev1.Move])] = List((
                                                  //| Block(Pos(4,7),Pos(4,7)),List(Right, Down, Right, Right, Down, Down, Right))
                                                  //| )
  
  (lev1.neighborsWithHistory(lev1.startBlock, Nil) take 3).toList
                                                  //> res7: List[(streams.test.lev1.Block, List[streams.test.lev1.Move])] = List((
                                                  //| Block(Pos(2,1),Pos(3,1)),List(Down)), (Block(Pos(1,2),Pos(1,3)),List(Right))
                                                  //| )
  (lev1.newNeighborsOnly(lev1.neighborsWithHistory(lev1.startBlock, Nil), Set()) take 3).toList
                                                  //> res8: List[(streams.test.lev1.Block, List[streams.test.lev1.Move])] = List((
                                                  //| Block(Pos(2,1),Pos(3,1)),List(Down)), (Block(Pos(1,2),Pos(1,3)),List(Right))
                                                  //| )
  (lev1.from(List((lev1.startBlock, Nil)).toStream, Set()) take 3).toList
                                                  //> res9: List[(streams.test.lev1.Block, List[streams.test.lev1.Move])] = List((
                                                  //| Block(Pos(1,1),Pos(1,1)),List()), (Block(Pos(2,1),Pos(3,1)),List(Down)), (Bl
                                                  //| ock(Pos(1,2),Pos(1,3)),List(Right)))
}