package funsets

object Main extends App {
  import FunSets._
  println(contains(singletonSet(1), 1))
	val s3 = singletonSet(3)
	def oneAndTwo: Set = (x => (x >= 1 && x <= 2));
	for (i <- -5 to 5) 
	println(i + ". " + (s3(i) && exists(x => true, (x => x == i + 1))))
  
}
