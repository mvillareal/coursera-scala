package funsets
import FunSets._

object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  def addOne(x:Int) : Int => Int = (x => x+1)     //> addOne: (x: Int)Int => Int
  def oneAndTwo: Set = (x => (x >= 1 && x <= 2))  //> oneAndTwo: => Int => Boolean
  val s3 = singletonSet(3)                        //> s3  : Int => Boolean = <function1>
  
  def m(s: Set, f: Int => Int): Set = x => exists(s, y => (f(y) == x))
                                                  //> m: (s: Int => Boolean, f: Int => Int)Int => Boolean
   
  printSet(m(oneAndTwo, x => x))                  //> {1,2}
  val a = m(s3, x => x + 1)                       //> a  : Int => Boolean = <function1>
  printSet(a)                                     //> {4}
  
}